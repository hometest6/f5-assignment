import pytest
from urllib.request import urlopen
from bs4 import BeautifulSoup

@pytest.fixture
def setup():
    url = "https://the-internet.herokuapp.com/context_menu"
    # get the HTML of website
    html = urlopen(url).read()
    # extract all the text field of the page
    text = ' '.join(BeautifulSoup(html, "html.parser").stripped_strings)
    
    yield text


def test_1(setup) -> None:

    s = "Right-click in the box below to see one called 'the-internet'"
    text = setup
    assert s in text


def test_2(setup) -> None:
    try:
          s = "Alibaba"
          text = setup
          assert s in text
    except Exception:
        print("failed")      
        
  

